package id.co.naupal.newsflash.source

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import id.co.naupal.newsflash.R
import id.co.naupal.newsflash.app.data.model.Source
import id.co.naupal.newsflash.app.mvp.BaseActivity
import id.co.naupal.newsflash.app.mvp.IBasePresenter
import id.co.naupal.newsflash.articles.ArticlesActivity
import kotlinx.android.synthetic.main.activity_source.*
import kotlinx.android.synthetic.main.item_source.view.*
import javax.inject.Inject

/**
 * Created by Naupal T. on 7/3/18.
 */
class SourceActivity : BaseActivity(), SourceContract.View {
    @Inject
    lateinit var presenter: SourceContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_source)
        listSource.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        listSource.adapter = SourceAdapter(presenter.sources, this)
        presenter.getData()
    }

    override fun getPresenter(): IBasePresenter {
        return presenter
    }

    override fun refreshUi() {
        Log.i("TAG", "refresh UIIII")
        listSource.adapter.notifyDataSetChanged()
    }

    override fun showMessage(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }


    class SourceAdapter(private val items: List<Source>, private val context: Context) : RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_source, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.name.text = items[position].name
            holder.url.text = items[position].url
            holder.layout.setOnClickListener({
                val intent = Intent(context, ArticlesActivity::class.java)
                intent.putExtra("source", items[position])
                context.startActivity(intent)
            })
        }

        override fun getItemCount(): Int {
            return items.size
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name = view.txt_title!!
        val layout = view.lay_item!!
        val url = view.txt_web_url!!
    }
}