package id.co.naupal.newsflash.app.data.api

import id.co.naupal.newsflash.app.data.model.ArticleResponse
import id.co.naupal.newsflash.app.data.model.SourceResponse
import javax.inject.Inject

/**
 * Created by Naupal T. on 7/3/18.
 */


class ApiService
@Inject constructor(private val api: ApiRetrofit) : IApiService {

    override fun getSources(): SourceResponse? {
        return api
                .getResources()
                .execute()
                .body()
    }

    override fun getArticles(source: String, keyword: String): ArticleResponse? {
        return api
                .getArticles(source, keyword)
                .execute()
                .body()
    }
}