package id.co.naupal.newsflash.app.mvp

import android.view.View

/**
 * Created by Naupal T. on 7/3/18.
 */


interface IBaseView {
    fun getPresenter(): IBasePresenter

    fun showProgress(msg: String?, cancelable: Boolean)

    fun hideProgress()

    fun showMessage(msg: String)

}