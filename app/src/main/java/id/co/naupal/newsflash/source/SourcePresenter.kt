package id.co.naupal.newsflash.source

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import id.co.naupal.newsflash.app.data.model.Source
import id.co.naupal.newsflash.app.mvp.BasePresenter
import id.co.naupal.newsflash.app.mvp.IBaseInteractor
import javax.inject.Inject

/**
 * Created by Naupal T. on 7/3/18.
 */

class SourcePresenter @Inject constructor() : BasePresenter(), SourceContract.Presenter {

    override var sources: List<Source>
        get() = daysForecast
        set(value) {}

    private val daysForecast: MutableList<Source> = mutableListOf()

    @Inject
    lateinit var view: SourceContract.View

    @Inject
    lateinit var interactor: SourceContract.Interactor

    override fun getInteractor(): IBaseInteractor = interactor

    @Synchronized
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private fun onViewCreate() {

    }

    override fun getData() = launchOnUITryCatch({
        view.showProgress("Filling Data", false)
        val srcs: List<Source>? = interactor.getSource()
        daysForecast.clear()
        if (srcs != null) {
            daysForecast.addAll(srcs)
        }
        view.hideProgress()
        view.refreshUi()
    }, {
        view.hideProgress()
        view.showMessage("Get data error!")
    }, false)

}