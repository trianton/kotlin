package id.co.naupal.newsflash.app.mvp

import android.support.annotation.CallSuper
import id.co.naupal.newsflash.app.data.model.Source
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.CoroutineScope
import kotlinx.coroutines.experimental.Deferred

/**
 * Created by Naupal T. on 7/3/18.
 */


abstract class BaseInteractor : IBaseInteractor {
    private val deferredObjects: MutableList<Deferred<*>> = mutableListOf()

    override fun onPause() {
    }

    override fun onResume() {
    }

    @CallSuper
    @Synchronized
    protected suspend fun <T> async(block: suspend CoroutineScope.() -> T): Deferred<T> {
        val deferred: Deferred<T> = kotlinx.coroutines.experimental.async(CommonPool) { block() }
        deferredObjects.add(deferred)
        deferred.invokeOnCompletion { deferredObjects.remove(deferred) }
        return deferred
    }

    @CallSuper
    @Synchronized
    protected suspend fun <T> asyncAwait(block: suspend CoroutineScope.() -> T): T {
        return async(block).await()
    }

    @CallSuper
    @Synchronized
    protected fun cancelAllAsync() {
        val deferredObjectsSize = deferredObjects.size

        if (deferredObjectsSize > 0) {
            for (i in deferredObjectsSize - 1 downTo 0) {
                deferredObjects[i].cancel()
            }
        }
    }

    @CallSuper
    @Synchronized
    open fun cleanup() {
        cancelAllAsync()
    }
}