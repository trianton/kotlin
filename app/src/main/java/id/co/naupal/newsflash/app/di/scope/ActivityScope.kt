package id.co.naupal.newsflash.app.di.scope

import javax.inject.Scope

/**
 * Created by Naupal T. on 7/3/18.
 */

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope