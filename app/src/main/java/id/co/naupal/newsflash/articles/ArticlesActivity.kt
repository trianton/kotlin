package id.co.naupal.newsflash.articles

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.ArrayAdapter
import android.widget.Toast
import com.bumptech.glide.Glide
import id.co.naupal.newsflash.R
import id.co.naupal.newsflash.app.data.model.Article
import id.co.naupal.newsflash.app.data.model.Source
import id.co.naupal.newsflash.app.helper.DateHelper
import id.co.naupal.newsflash.app.mvp.BaseActivity
import id.co.naupal.newsflash.app.mvp.IBasePresenter
import id.co.naupal.newsflash.article.WebViewActivity
import kotlinx.android.synthetic.main.activity_source.*
import kotlinx.android.synthetic.main.item_article.view.*
import javax.inject.Inject


/**
 * Created by Naupal T. on 7/3/18.
 */
class ArticlesActivity : BaseActivity(), ArticlesContract.View {
    @Inject
    lateinit var presenter: ArticlesContract.Presenter
    lateinit var source: Source
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_source)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val extras = intent.extras
        if (extras != null) {
            source = extras.get("source") as Source
            supportActionBar?.title = source.name
            listSource.layoutManager = LinearLayoutManager(this)
            listSource.adapter = ArticleAdapter(presenter.articles, this, source)
            presenter.getData(source.id!!, "")
        } else {
            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the search menu action bar.
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.actionbar_articles, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        } else if (item.itemId == R.id.app_bar_menu_search) {
            val searchView = MenuItemCompat.getActionView(item) as SearchView
            val searchAutoComplete = searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text) as SearchView.SearchAutoComplete
//            searchAutoComplete.setBackgroundColor(Color.BLUE)
//            searchAutoComplete.setTextColor(Color.GREEN)
            searchAutoComplete.setDropDownBackgroundResource(android.R.color.white)

            // Create a new ArrayAdapter and add data to search auto complete object.
            val dataArr = arrayOf("Apple", "Amazon", "Amd", "Microsoft", "Microwave", "MicroNews", "Intel", "Intelligence")
            val newsAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, dataArr)
            searchAutoComplete.setAdapter(newsAdapter)

            // Listen to search view item on click event.
            searchAutoComplete.setOnItemClickListener({ adapterView, view, itemIndex, id ->
                val queryString = adapterView.getItemAtPosition(itemIndex) as String
                searchAutoComplete.setText("" + queryString)
                Toast.makeText(this@ArticlesActivity, "you clicked $queryString", Toast.LENGTH_LONG).show()
            })

            // Below event is triggered when submit search query.
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    presenter.getData(source.id!!, query)
                    return false
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    return false
                }
            })
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getPresenter(): IBasePresenter {
        return presenter
    }

    override fun refreshUi() {
        listSource.adapter.notifyDataSetChanged()
    }

    override fun showMessage(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress(msg: String?, cancelable: Boolean) {
        progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress.visibility = View.GONE
    }


    class ArticleAdapter(private val items: List<Article>, private val context: Context, private val source: Source) : RecyclerView.Adapter<ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_article, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder?.name?.text = items.get(position).title
            holder?.time.text = DateHelper.getDateTimeDefault(items[position].publishedAt, source.language!!, source.country!!)
            holder?.layout.setOnClickListener({
                val intent = Intent(context, WebViewActivity::class.java)
                intent.putExtra("url", items.get(position).url)
                context.startActivity(intent)
            })
            Glide.with(context)
                    .load(items.get(position).urlToImage)
                    .into(holder.img)
        }

        override fun getItemCount(): Int {
            return items.size
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name = view.txt_title_article
        val layout = view.lay_item_article
        val img = view.img_news
        val time = view.text_time
    }


}