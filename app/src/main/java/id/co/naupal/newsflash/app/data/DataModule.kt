package id.co.naupal.newsflash.app.data

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import id.co.naupal.newsflash.app.App
import id.co.naupal.newsflash.app.data.api.ApiRetrofit
import id.co.naupal.newsflash.app.data.api.ApiService
import id.co.naupal.newsflash.app.data.api.IApiService
import id.co.naupal.newsflash.app.data.pref.IPrefHelper
import id.co.naupal.newsflash.app.data.pref.PrefHelper
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by Naupal T. on 7/3/18.
 */

@Module
class DataModule {
    @Provides
    @Singleton
    fun providePreference(app: App): IPrefHelper = PrefHelper(app)

    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create()

    @Provides
    @Singleton
    fun provideOkHttpClient(app: App): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val interceptorHeader = Interceptor { chain ->
            chain.proceed(chain.request()
                    .newBuilder()
                    .addHeader("X-Api-Key", "7f09ab859b664411aaef338993385625")
                    .build())
        }

        val cacheDir = File(app.cacheDir, UUID.randomUUID().toString())
        val cache = Cache(cacheDir, 10 * 1024 * 1024)

        return OkHttpClient.Builder()
                .cache(cache)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .addInterceptor(interceptorHeader)
                .build()
    }

    @Provides
    @Singleton
    fun provideApiRetrofit(gson: Gson, okHttpClient: OkHttpClient): ApiRetrofit = Retrofit.Builder()
            .baseUrl("https://newsapi.org/v2/")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(ApiRetrofit::class.java)


    @Provides
    @Singleton
    internal fun provideApiService(apiService: ApiService): IApiService {
        return apiService
    }
}