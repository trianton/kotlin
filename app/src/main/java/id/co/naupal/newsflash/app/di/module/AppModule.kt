package id.co.naupal.newsflash.app.di.module

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Naupal T. on 7/3/18.
 */

@Module
class AppModule {
    @Provides
    @Singleton
    fun provideApplication(application: Application) = application
}