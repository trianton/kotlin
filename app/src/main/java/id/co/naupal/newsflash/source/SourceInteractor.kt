package id.co.naupal.newsflash.source

import id.co.naupal.newsflash.app.data.api.IApiService
import id.co.naupal.newsflash.app.data.model.Source
import id.co.naupal.newsflash.app.mvp.BaseInteractor
import javax.inject.Inject

/**
 * Created by Naupal T. on 7/3/18.
 */

class SourceInteractor
@Inject constructor(private val api: IApiService)
    : BaseInteractor(), SourceContract.Interactor {

    override suspend fun getSource(): List<Source>? {
        return asyncAwait { (api.getSources()?.source) }
    }
}