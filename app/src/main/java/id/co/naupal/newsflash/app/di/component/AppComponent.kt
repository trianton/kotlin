package id.co.naupal.newsflash.app.di.component

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import id.co.naupal.newsflash.app.App
import id.co.naupal.newsflash.app.data.DataModule
import id.co.naupal.newsflash.articles.di.ArticlesModule
import id.co.naupal.newsflash.source.di.SourceModule
import id.co.naupal.newsflash.splash.di.SplashModule
import javax.inject.Singleton

/**
 * Created by Naupal T. on 7/3/18.
 */

@Singleton
@Component(modules = [AndroidInjectionModule::class, DataModule::class, SplashModule::class, SourceModule::class, ArticlesModule::class])
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: App): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}