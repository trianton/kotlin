package id.co.naupal.newsflash.articles.di

import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.co.naupal.newsflash.app.di.scope.ActivityScope
import id.co.naupal.newsflash.articles.ArticlesActivity
import id.co.naupal.newsflash.articles.ArticlesContract
import id.co.naupal.newsflash.articles.ArticlesPresenter

/**
 * Created by Naupal T. on 7/3/18.
 */

@Module
abstract class ArticlesModule {
    @ActivityScope
    @ContributesAndroidInjector
    (modules = [(ArticlesDependenModule::class)])
    abstract fun contibutesSourceActivity(): ArticlesActivity

    @ActivityScope
    @Binds
    abstract fun providePresenter(presenter: ArticlesPresenter): ArticlesContract.Presenter

    @ActivityScope
    @Binds
    abstract fun provideView(view: ArticlesActivity): ArticlesContract.View
}