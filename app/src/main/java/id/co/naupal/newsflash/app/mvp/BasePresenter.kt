package id.co.naupal.newsflash.app.mvp

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import android.arch.lifecycle.ViewModel
import android.support.annotation.CallSuper
import android.util.Log
import kotlinx.coroutines.experimental.CoroutineScope
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import java.util.concurrent.CancellationException

/**
 * Created by Naupal T. on 7/3/18.
 */

abstract class BasePresenter : IBasePresenter, ViewModel() {

    private val asyncJobs: MutableList<Job> = mutableListOf()
    private var viewLifecycle: Lifecycle? = null

    override fun onCleared() {
        cleanup()
        super.onCleared()
    }

    @Synchronized
    override fun attachView(view: IBaseView, viewLifecycle: Lifecycle) {
        this.viewLifecycle = viewLifecycle
    }

    @Synchronized
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private fun onViewStarted() {
        Log.i("BasePresenterImpl", "ON_START")
    }

    @Synchronized
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private fun onViewPause() {
        Log.i("BasePresenterImpl", "ON_PAUSE")
        getInteractor().onPause()
    }

    @Synchronized
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun onViewResume() {
        Log.i("BasePresenterImpl", "ON_RESUME")
        getInteractor().onResume()
    }

    @CallSuper
    @Synchronized
    protected fun launchAsync(block: suspend CoroutineScope.() -> Unit) {
        val job: Job = launch(UI) { block() }
        asyncJobs.add(job)
        job.invokeOnCompletion { asyncJobs.remove(job) }
    }


    @CallSuper
    @Synchronized
    private fun cancelAllAsync() {
        val asyncJobsSize = asyncJobs.size
        if (asyncJobsSize > 0) {
            for (i in asyncJobsSize - 1 downTo 0) {
                asyncJobs[i].cancel()
            }
        }
    }

    @CallSuper
    @Synchronized
    open fun cleanup() {
        cancelAllAsync()
    }


    @Synchronized
    fun launchOnUI(block: suspend CoroutineScope.() -> Unit) {
        val job: Job = launch(UI) { block() }
        asyncJobs.add(job)
        job.invokeOnCompletion { asyncJobs.remove(job) }
    }

    @Synchronized
    fun launchOnUITryCatch(
            tryBlock: suspend CoroutineScope.() -> Unit,
            catchBlock: suspend CoroutineScope.(Throwable) -> Unit,
            handleCancellationExceptionManually: Boolean) {
        launchOnUI { tryCatch(tryBlock, catchBlock, handleCancellationExceptionManually) }
    }

    suspend fun CoroutineScope.tryCatch(
            tryBlock: suspend CoroutineScope.() -> Unit,
            catchBlock: suspend CoroutineScope.(Throwable) -> Unit,
            handleCancellationExceptionManually: Boolean = false) {
        try {
            tryBlock()
        } catch (e: Throwable) {
            if (e !is CancellationException || handleCancellationExceptionManually) {
                catchBlock(e)
            } else {
                throw e
            }
        }
    }

}