package id.co.naupal.newsflash.splash.di

import dagger.Module
import dagger.Provides
import id.co.naupal.newsflash.app.data.api.IApiService
import id.co.naupal.newsflash.app.data.pref.IPrefHelper
import id.co.naupal.newsflash.app.di.scope.ActivityScope
import id.co.naupal.newsflash.splash.SplashContract
import id.co.naupal.newsflash.splash.SplashInteractor

/**
 * Created by Naupal T. on 7/3/18.
 */


@Module
class SplashDependModule {
    @Provides
    @ActivityScope
    fun provideInteractor(pref: IPrefHelper, api: IApiService):
            SplashContract.Interactor = SplashInteractor(pref, api)

}