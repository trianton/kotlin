package id.co.naupal.newsflash.articles

import id.co.naupal.newsflash.app.data.model.Article
import id.co.naupal.newsflash.app.mvp.IBaseInteractor
import id.co.naupal.newsflash.app.mvp.IBasePresenter
import id.co.naupal.newsflash.app.mvp.IBaseView

/**
 * Created by Naupal T. on 7/3/18.
 */

interface ArticlesContract {

    interface View : IBaseView {
        fun refreshUi()
    }

    interface Presenter : IBasePresenter {
        var articles: List<Article>
        fun getData(source: String, keyword: String)
    }

    interface Interactor : IBaseInteractor {
        suspend fun getArticles(source: String, keyword: String): List<Article>?
    }
}