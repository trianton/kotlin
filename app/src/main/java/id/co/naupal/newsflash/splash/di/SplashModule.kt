package id.co.naupal.newsflash.splash.di

import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.co.naupal.newsflash.app.di.scope.ActivityScope
import id.co.naupal.newsflash.splash.SplashActivity
import id.co.naupal.newsflash.splash.SplashContract
import id.co.naupal.newsflash.splash.SplashPresenter

/**
 * Created by Naupal T. on 7/3/18.
 */


@Module
abstract class SplashModule {
    @ActivityScope
    @ContributesAndroidInjector
    (modules = [(SplashDependModule::class)])
    abstract fun contibutesSplashActivity(): SplashActivity

    @ActivityScope
    @Binds
    abstract fun providePresenter(presenter: SplashPresenter): SplashContract.Presenter

    @ActivityScope
    @Binds
    abstract fun provideView(view: SplashActivity): SplashContract.View
}