package id.co.naupal.newsflash.splash

import android.content.Intent
import android.view.View
import android.widget.Toast
import id.co.naupal.newsflash.R
import id.co.naupal.newsflash.app.mvp.BaseActivity
import id.co.naupal.newsflash.app.mvp.IBasePresenter
import id.co.naupal.newsflash.source.SourceActivity
import javax.inject.Inject

/**
 * Created by Naupal T. on 7/3/18.
 */

class SplashActivity : BaseActivity(), SplashContract.View {
    override fun showMessage(msg: String) {
        Toast.makeText(this, getString(R.string.there_are_something_error), Toast.LENGTH_SHORT).show()
    }

    @Inject
    lateinit var presenter: SplashContract.Presenter

    override fun getPresenter(): IBasePresenter = presenter

    override fun onResume() {
        super.onResume()
        presenter.routing()
    }

    override fun openLoginPage() {
        val intent = Intent(this, SourceActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        finish()
    }

    override fun openDashboardPage() {
        //TODO go to dashboard
    }
}