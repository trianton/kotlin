package id.co.naupal.newsflash.app.data.api

import id.co.naupal.newsflash.app.data.model.ArticleResponse
import id.co.naupal.newsflash.app.data.model.SourceResponse

/**
 * Created by Naupal T. on 7/3/18.
 */

interface IApiService {
    fun getSources(): SourceResponse?

    fun getArticles(source: String, keyword: String): ArticleResponse?
}