package id.co.naupal.newsflash.app.mvp

import android.os.Bundle
import android.support.v4.app.Fragment

/**
 * Created by Naupal T. on 7/3/18.
 */

abstract class BaseFragment : Fragment(), IBaseView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupPresenter()
    }

    private fun setupPresenter() {
        getPresenter().attachView(this,lifecycle)
        lifecycle.addObserver(getPresenter())
    }
}