package id.co.naupal.newsflash.app.mvp

/**
 * Created by Naupal T. on 7/3/18.
 */

interface IBaseInteractor {
    fun onResume()

    fun onPause()
}