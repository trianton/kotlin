package id.co.naupal.newsflash.articles

import id.co.naupal.newsflash.app.data.api.IApiService
import id.co.naupal.newsflash.app.data.model.Article
import id.co.naupal.newsflash.app.mvp.BaseInteractor
import javax.inject.Inject

/**
 * Created by Naupal T. on 7/3/18.
 */

class ArticlesInteractor
@Inject constructor(private val api: IApiService)
    : BaseInteractor(), ArticlesContract.Interactor {
    override suspend fun getArticles(source: String, keyword: String): List<Article>? {
        return asyncAwait { (api.getArticles(source, keyword)?.source) }
    }
}