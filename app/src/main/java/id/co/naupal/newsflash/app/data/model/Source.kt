package id.co.naupal.newsflash.app.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by Naupal T. on 7/3/18.
 */

@Parcelize
data class Source(
        @SerializedName("id") val id: String?,
        @SerializedName("name") val name: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("url") val url: String?,
        @SerializedName("category") val category: String?,
        @SerializedName("language") val language: String?,
        @SerializedName("country") val country: String?) : Parcelable