package id.co.naupal.newsflash.articles.di

import dagger.Module
import dagger.Provides
import id.co.naupal.newsflash.app.data.api.IApiService
import id.co.naupal.newsflash.app.di.scope.ActivityScope
import id.co.naupal.newsflash.articles.ArticlesContract
import id.co.naupal.newsflash.articles.ArticlesInteractor
import id.co.naupal.newsflash.source.SourceContract
import id.co.naupal.newsflash.source.SourceInteractor

/**
 * Created by Naupal T. on 7/3/18.
 */

@Module
class ArticlesDependenModule {
    @Provides
    @ActivityScope
    fun provideInteractor(api: IApiService): ArticlesContract.Interactor = ArticlesInteractor(api)

}