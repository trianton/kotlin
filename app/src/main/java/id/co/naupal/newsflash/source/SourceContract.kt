package id.co.naupal.newsflash.source

import id.co.naupal.newsflash.app.data.model.Source
import id.co.naupal.newsflash.app.mvp.IBaseInteractor
import id.co.naupal.newsflash.app.mvp.IBasePresenter
import id.co.naupal.newsflash.app.mvp.IBaseView

/**
 * Created by Naupal T. on 7/3/18.
 */

interface SourceContract {

    interface View : IBaseView {
        fun refreshUi()
    }

    interface Presenter : IBasePresenter {
        var sources: List<Source>
        fun getData()
    }

    interface Interactor : IBaseInteractor {
        suspend fun getSource(): List<Source>?
    }
}