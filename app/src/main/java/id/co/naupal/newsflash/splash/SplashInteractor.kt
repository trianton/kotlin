package id.co.naupal.newsflash.splash

import id.co.naupal.newsflash.app.data.api.IApiService
import id.co.naupal.newsflash.app.data.pref.IPrefHelper
import id.co.naupal.newsflash.app.mvp.BaseInteractor
import javax.inject.Inject

/**
 * Created by Naupal T. on 7/3/18.
 */

class SplashInteractor
@Inject constructor(private val pref: IPrefHelper, private val api: IApiService)
    : BaseInteractor(), SplashContract.Interactor {

    override fun isDataLoaded() = pref.isDataLoaded()

    override suspend fun isLoggedIn(): Boolean {
        asyncAwait {
            simulateSlowNetwork()
        }
        return pref.isLoggedIn()
    }

    private suspend fun simulateSlowNetwork() {
//        delay(1000 + Random().nextInt(1000).toLong())
//        Log.i("API NIH", api.getSources().toString())
    }
}