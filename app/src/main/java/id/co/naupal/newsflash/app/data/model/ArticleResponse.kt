package id.co.naupal.newsflash.app.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Naupal T. on 7/3/18.
 */

data class ArticleResponse(
        @SerializedName("status") val status: String?,
        @SerializedName("articles") val source: ArrayList<Article>?)

