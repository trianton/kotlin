package id.co.naupal.newsflash.articles

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import id.co.naupal.newsflash.app.data.model.Article
import id.co.naupal.newsflash.app.mvp.BasePresenter
import id.co.naupal.newsflash.app.mvp.IBaseInteractor
import javax.inject.Inject

/**
 * Created by Naupal T. on 7/3/18.
 */

class ArticlesPresenter @Inject constructor() : BasePresenter(), ArticlesContract.Presenter {

    override var articles: List<Article>
        get() = articlesMutable
        set(value) {}

    private val articlesMutable: MutableList<Article> = mutableListOf()

    @Inject
    lateinit var view: ArticlesContract.View

    @Inject
    lateinit var interactor: ArticlesContract.Interactor

    override fun getInteractor(): IBaseInteractor = interactor

    @Synchronized
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private fun onViewCreate() {

    }

    override fun getData(source: String, keyword: String) = launchOnUITryCatch({
        view.showProgress("Filling Data", false)
        val srcs: List<Article>? = interactor.getArticles(source, keyword)
        if (srcs != null) {
            articlesMutable.clear()
            articlesMutable.addAll(srcs)
        }
        view.hideProgress()
        view.refreshUi()
    }, {
        view.hideProgress()
        view.showMessage("Filling data error!")
    }, false)

}