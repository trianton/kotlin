package id.co.naupal.newsflash.source.di

import dagger.Module
import dagger.Provides
import id.co.naupal.newsflash.app.data.api.IApiService
import id.co.naupal.newsflash.app.di.scope.ActivityScope
import id.co.naupal.newsflash.source.SourceContract
import id.co.naupal.newsflash.source.SourceInteractor

/**
 * Created by Naupal T. on 7/3/18.
 */

@Module
class SourceDependenModule {
    @Provides
    @ActivityScope
    fun provideInteractor(api: IApiService): SourceContract.Interactor = SourceInteractor(api)

}