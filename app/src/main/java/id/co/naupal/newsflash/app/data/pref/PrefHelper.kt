package id.co.naupal.newsflash.app.data.pref

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Naupal T. on 7/3/18.
 */

@Singleton
class PrefHelper @Inject constructor(context: Context) : IPrefHelper {
    companion object {
        private const val PREF_BUFFER_PACKAGE_NAME = "id.co.naupal.newsflash"
        private const val LOGGED_IN = "logged_in"
        private const val DATA_LOADED = "data_loaded"
    }

    private val bufferPref: SharedPreferences

    init {
        bufferPref = context.getSharedPreferences(PREF_BUFFER_PACKAGE_NAME, Context.MODE_PRIVATE)
    }

    override fun isLoggedIn() = bufferPref.getBoolean(LOGGED_IN, false)

    override fun setLoggedIn(isBoolean: Boolean) {
        bufferPref.edit().putBoolean(LOGGED_IN, isBoolean).apply()
    }

    override fun isDataLoaded() = bufferPref.getBoolean(DATA_LOADED, false)

    override fun setDataLoaded(isBoolean: Boolean) {
        bufferPref.edit().putBoolean(DATA_LOADED, isBoolean).apply()
    }

}