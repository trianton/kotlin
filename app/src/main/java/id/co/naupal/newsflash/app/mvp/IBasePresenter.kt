package id.co.naupal.newsflash.app.mvp

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver

/**
 * Created by Naupal T. on 7/3/18.
 */


interface IBasePresenter : LifecycleObserver {
    fun getInteractor(): IBaseInteractor
    fun attachView(view: IBaseView, viewLifecycle: Lifecycle)
}