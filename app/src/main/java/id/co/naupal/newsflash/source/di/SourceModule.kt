package id.co.naupal.newsflash.source.di

import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.co.naupal.newsflash.app.di.scope.ActivityScope
import id.co.naupal.newsflash.source.SourceActivity
import id.co.naupal.newsflash.source.SourceContract
import id.co.naupal.newsflash.source.SourcePresenter

/**
 * Created by Naupal T. on 7/3/18.
 */

@Module
abstract class SourceModule {
    @ActivityScope
    @ContributesAndroidInjector
    (modules = [(SourceDependenModule::class)])
    abstract fun contibutesSourceActivity(): SourceActivity

    @ActivityScope
    @Binds
    abstract fun providePresenter(presenter: SourcePresenter): SourceContract.Presenter

    @ActivityScope
    @Binds
    abstract fun provideView(view: SourceActivity): SourceContract.View
}