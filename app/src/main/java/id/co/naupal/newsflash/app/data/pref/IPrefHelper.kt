package id.co.naupal.newsflash.app.data.pref

/**
 * Created by Naupal T. on 7/3/18.
 */

interface IPrefHelper {
    fun isLoggedIn(): Boolean
    fun setLoggedIn(isLoggedIn: Boolean)

    fun isDataLoaded():Boolean
    fun setDataLoaded(isLoggedIn :Boolean)
}