package id.co.naupal.newsflash.splash

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import id.co.naupal.newsflash.app.mvp.BasePresenter
import id.co.naupal.newsflash.app.mvp.IBaseInteractor
import javax.inject.Inject

/**
 * Created by Naupal T. on 7/3/18.
 */

class SplashPresenter @Inject constructor() : BasePresenter(), SplashContract.Presenter {
    @Inject
    lateinit var view: SplashContract.View

    @Inject
    lateinit var interactor: SplashContract.Interactor

    override fun getInteractor(): IBaseInteractor = interactor

    @Synchronized
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private fun onViewCreate() {
        if (interactor.isDataLoaded()) {
            view.openDashboardPage()
        }
    }

    override fun routing() {
        launchAsync {
            if (!interactor.isLoggedIn()) {
                view.openLoginPage()
            }
        }
    }
}