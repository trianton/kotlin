package id.co.naupal.newsflash.app.data.api

import id.co.naupal.newsflash.app.data.model.ArticleResponse
import id.co.naupal.newsflash.app.data.model.SourceResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Naupal T. on 7/3/18.
 */

interface ApiRetrofit {

    @GET("sources")
    fun getResources(): Call<SourceResponse>

    @GET("everything")
    fun getArticles(@Query("sources") source: String, @Query("q") keyword: String): Call<ArticleResponse>
}