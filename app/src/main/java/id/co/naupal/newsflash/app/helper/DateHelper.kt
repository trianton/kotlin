package id.co.naupal.newsflash.app.helper

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Naupal T. on 7/4/18.
 */

class DateHelper {
    companion object {
        fun getDateTimeDefault(tz: String?, lang: String, country: String): String {
            val locSource = Locale(lang, country)
            val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", locSource)
            var dateTime: Long? = 0L
            try {
                val date = sdf.parse(tz)
                dateTime = date.time
            } catch (e: Exception) {
                e.printStackTrace()
            }

            val dt = Calendar.getInstance()
            dt.timeInMillis = dateTime!!
            val id = Locale("in", "ID")


            val sdfLocale = SimpleDateFormat("EEEE, dd MMM yyyy, HH:mm", id)
            sdfLocale.timeZone = dt.timeZone
            val res = sdfLocale.format(dt.time)

            return res + " " + getExtension(dt)
        }

        private fun getExtension(calendar: Calendar): String {
            return if (calendar.timeZone.displayName.equals("Western Indonesia Time", ignoreCase = true)) {
                "WIB"
            } else if (calendar.timeZone.displayName.equals("Eastern Indonesian Time", ignoreCase = true)) {
                "WIT"
            } else if (calendar.timeZone.displayName.equals("Central Indonesian Time", ignoreCase = true)) {
                "WITA"
            } else {
                ""
            }
        }
    }


}