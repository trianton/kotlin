package id.co.naupal.newsflash.app.mvp

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import dagger.android.AndroidInjection

/**
 * Created by Naupal T. on 7/3/18.
 */

abstract class BaseActivity : AppCompatActivity(), IBaseView {
    var mProgressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setupPresenter()
    }

    private fun setupPresenter() {
        getPresenter().attachView(this, lifecycle)
        lifecycle.addObserver(getPresenter())
    }

    override fun showProgress(msg: String?, cancelable: Boolean) {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog(this)
            mProgressDialog?.setIndeterminate(true)
        }
        mProgressDialog?.setMessage(msg ?: "")
        mProgressDialog?.setCancelable(cancelable)
        mProgressDialog?.setCanceledOnTouchOutside(cancelable)
        mProgressDialog?.show()
    }

    override fun hideProgress() {
        if (mProgressDialog != null && mProgressDialog?.isShowing()!!) {
            mProgressDialog?.dismiss()
        }
    }


}