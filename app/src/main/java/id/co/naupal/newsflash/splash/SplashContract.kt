package id.co.naupal.newsflash.splash

import id.co.naupal.newsflash.app.mvp.IBaseInteractor
import id.co.naupal.newsflash.app.mvp.IBasePresenter
import id.co.naupal.newsflash.app.mvp.IBaseView

/**
 * Created by Naupal T. on 7/3/18.
 */

interface SplashContract {

    interface View : IBaseView {
        fun openLoginPage()
        fun openDashboardPage()
    }

    interface Presenter : IBasePresenter {
        fun routing()
    }

    interface Interactor : IBaseInteractor {
        fun isDataLoaded(): Boolean
        suspend fun isLoggedIn(): Boolean
    }
}